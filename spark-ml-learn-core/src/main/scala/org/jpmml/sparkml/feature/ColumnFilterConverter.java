package org.jpmml.sparkml.feature;

import org.apache.spark.ml.feature.ColumnFilter;
import org.jpmml.converter.Feature;
import org.jpmml.sparkml.FeatureConverter;
import org.jpmml.sparkml.SparkMLEncoder;

import java.util.ArrayList;
import java.util.List;

public class ColumnFilterConverter extends FeatureConverter<ColumnFilter> {

    public ColumnFilterConverter(ColumnFilter transformer) {
        super(transformer);
    }

    @Override
    public List<Feature> encodeFeatures(SparkMLEncoder encoder){
        List<Feature> result = new ArrayList<>();
        String[] inputCols = getTransformer().getOutputCols();
        for(String inputCol : inputCols) {
            Feature feature = encoder.getOnlyFeature(inputCol);
            result.add(feature);
        }
        return result;
    }

}
