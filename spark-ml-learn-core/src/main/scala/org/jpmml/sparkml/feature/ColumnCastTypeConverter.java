package org.jpmml.sparkml.feature;

import org.apache.spark.ml.feature.ColumnCastType;
import org.apache.spark.ml.feature.ColumnFilter;
import org.jpmml.converter.Feature;
import org.jpmml.sparkml.FeatureConverter;
import org.jpmml.sparkml.SparkMLEncoder;

import java.util.ArrayList;
import java.util.List;

public class ColumnCastTypeConverter extends FeatureConverter<ColumnCastType> {

    public ColumnCastTypeConverter(ColumnCastType transformer) {
        super(transformer);
    }

    @Override
    public List<Feature> encodeFeatures(SparkMLEncoder encoder){
        List<Feature> result = new ArrayList<>();
        String[] inputCols = getTransformer().getCastColumnsName();
        for(String inputCol : inputCols) {
            Feature feature = encoder.getOnlyFeature(inputCol);
            result.add(feature);
        }
        return result;
    }

}