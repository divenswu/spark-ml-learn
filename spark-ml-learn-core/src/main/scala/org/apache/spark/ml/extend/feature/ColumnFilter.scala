package org.apache.spark.ml.extend.feature

import org.apache.spark.ml.Transformer
import org.apache.spark.ml.param.ParamMap
import org.apache.spark.ml.param.shared.HasOutputCols
import org.apache.spark.ml.util.{DefaultParamsWritable, Identifiable, MLWritable}
import org.apache.spark.sql.types.{MetadataBuilder, StructField, StructType}
import org.apache.spark.sql.{DataFrame, Dataset}

class ColumnFilter(override val uid: String) extends Transformer with HasOutputCols with DefaultParamsWritable{

  def this() = this(Identifiable.randomUID("ColumnFilter"))

  def setFilterColumns(value: Array[String]): this.type = set(outputCols, value)

  override def transform(dataset: Dataset[_]): DataFrame = {
    dataset.select(${outputCols}.map(dataset.col): _*)
  }

  override def transformSchema(schema: StructType): StructType = {
    StructType(schema.fields.filter(col => ${outputCols}.contains(col.name)).map(item => {
      StructField(item.name, item.dataType, item.nullable, item.metadata)
    }))
  }

  override def copy(extra: ParamMap): ColumnFilter = defaultCopy(extra)

}

