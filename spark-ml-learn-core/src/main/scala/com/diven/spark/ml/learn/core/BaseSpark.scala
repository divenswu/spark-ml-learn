package com.diven.spark.ml.learn.core

import org.apache.spark.sql.{DataFrame, SparkSession}

abstract class BaseSpark {
    
    var personPath = "./spark-ml-learn-core/src/datas/person.csv"
    var irisPath = "./spark-ml-learn-core/src/datas/iris.csv"
    
    /**
     * 设置数据路径
     */
    def getDataPath(): String = {
        this.personPath
    }
    
    def getSparkSession(): SparkSession = {
        SparkSession.builder().appName(this.getClass.getSimpleName).master("local[3]").getOrCreate()
    }

    /**
     * 获取数据
     */
    def getDataFrame(sparkSession: SparkSession = this.getSparkSession()): DataFrame = {
        sparkSession.read.option("header", "true").option("inferSchema", "true").csv(getDataPath());
    }
    
    /**
     * 执行任务
     */
    def execute(dataFrame: DataFrame = this.getDataFrame())
}