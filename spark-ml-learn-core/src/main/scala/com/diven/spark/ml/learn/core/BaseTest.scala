package com.diven.spark.ml.learn.core

abstract class BaseTest {
  
    //实例
    def apply(): BaseSpark
    
    //运行
    def main(args: Array[String]): Unit = {
        apply().execute()
    }
    
}