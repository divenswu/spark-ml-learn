package com.kuainiu.beidou.engine.component.domain;

import java.io.Serializable;

/**
 * 协议接口类
 * @author diven
 */
public interface IProtocol<Ctx extends Serializable, In extends Serializable, St extends Serializable, Out extends Serializable> extends Serializable {

    /**
     *
     * @return
     */
    public Ctx getCtx();

    /**
     *
     * @return
     */
    public In getIn();

    /**
     *
     * @return
     */
    public St getSt();

    /**
     *
     * @return
     */
    public Out getOut();

}
