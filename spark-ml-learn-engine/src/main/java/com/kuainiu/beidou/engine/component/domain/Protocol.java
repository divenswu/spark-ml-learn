//package com.kuainiu.beidou.engine.component.domain;
//
//import com.kuainiu.beidou.domain.dataset.ds.Dataset;
//
//import java.io.Serializable;
//
///**
// * 数据协议
// * @param <St>
// */
//public class Protocol<St extends Serializable> implements IProtocol<Context, DataProtocol22, St, DataProtocol22>{
//
//    private Context ctx;
//
//    private DataProtocol22 in;
//
//    private St st;
//
//    private DataProtocol22 out;
//
//    @Override
//    public Context getCtx() {
//        return ctx;
//    }
//
//    public void setCtx(Context ctx) {
//        this.ctx = ctx;
//    }
//
//    @Override
//    public DataProtocol22 getIn() {
//        return in;
//    }
//
//    public void setIn(DataProtocol22 in) {
//        this.in = in;
//    }
//
//    @Override
//    public St getSt() {
//        return st;
//    }
//
//    public void setSt(St st) {
//        this.st = st;
//    }
//
//    @Override
//    public DataProtocol22 getOut() {
//        return out;
//    }
//
//    public void setOut(DataProtocol22 out) {
//        this.out = out;
//    }
//
//}
