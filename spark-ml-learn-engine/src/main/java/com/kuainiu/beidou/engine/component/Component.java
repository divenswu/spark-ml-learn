package com.kuainiu.beidou.engine.component;

import java.io.Serializable;

public interface Component<TrainParam extends Serializable, TransParams extends Serializable> extends Serializable {

    /**
     * 训练流程
     */
    String train(TrainParam params);

    /**
     * 数据转换流程
     */
    String transform(TransParams params);

}
