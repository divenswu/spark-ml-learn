package com.kuainiu.beidou.engine.component.domain

/**
 * 元数据信息
 * @param schemaItems
 */
class DataSchema (val schemaItems: Array[DataSchemaItem]) extends Serializable {

}
