package com.kuainiu.beidou.engine.component.domain

object LabelType extends Enumeration {

  type LabelType = Value

  val LABEL = Value("label")

  val FUNCTION = Value("function")

}
