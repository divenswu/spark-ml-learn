package com.kuainiu.beidou.engine.component.domain


/**
 * 文件类型
 */
object FileType extends Enumeration{

  type FileType = Value

  val JSON= Value("json")

  val CSV_WITH_HEADER = Value("csv")

  val CSV_WITHOUT_HEADER = Value("csv_without_header")

  val UNKNOWN= Value("unknown")

}