package com.kuainiu.beidou.engine.component.domain

/**
 * 机器学习类型
 */
sealed abstract class MsType(val sparkName: String, val javaName: String)

object MsType {

  val Continuous: MsType = {case object Continuous extends MsType("numeric","continuous"); Continuous}

  val Discrete: MsType = {case object Discrete extends MsType("nominal", "discrete"); Discrete}

  val TextValue: MsType = {case object TextValue extends MsType("unresolved", "textValue"); TextValue}

  val Unresolved: MsType = {case object Unresolved extends MsType("unresolved", "unresolved"); Unresolved}

  def fromName(name: String): MsType = {
    if (Continuous.sparkName == name || Continuous.javaName == name) {
      Continuous
    } else if (Discrete.sparkName == name || Discrete.javaName == name) {
      Discrete
    }else if (TextValue.javaName == name) {
      TextValue
    } else if (Unresolved.sparkName == name) {
      Unresolved
    } else {
      null
    }
  }

}