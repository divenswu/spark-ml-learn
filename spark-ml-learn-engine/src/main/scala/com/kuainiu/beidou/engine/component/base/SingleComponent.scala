package com.kuainiu.beidou.engine.component.base
import com.kuainiu.beidou.engine.component.domain.{DataInfo, DataInfos, MapParams}

abstract class SingleComponent extends SimpleComponent{
  /**
   * 训练流程
   */
  override def train(dataInfos: DataInfos, params: MapParams): String = {
    if(dataInfos != null && dataInfos.size() > 0 ){
      this.train(dataInfos.getFirst, params)
    }
    else{
      this.train(null.asInstanceOf[DataInfo], params)
    }
  }

  /**
   * 数据转换流程
   */
  override def transform(dataInfos: DataInfos, params: MapParams): String = {
    if(dataInfos != null && dataInfos.size() > 0 ){
      this.transform(dataInfos.getFirst, params)
    }
    else{
      this.transform(null.asInstanceOf[DataInfo], params)
    }
  }


  /**
   * 训练流程
   */
  def train(dataInfo: DataInfo, params: MapParams): String

  /**
   * 数据转换流程
   */
  def transform(dataInfo: DataInfo, params: MapParams): String
}
