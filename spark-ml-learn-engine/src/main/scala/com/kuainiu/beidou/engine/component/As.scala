package com.kuainiu.beidou.engine.component

import java.util.Spliterators
import java.util.stream.{Collectors, StreamSupport}

import com.alibaba.fastjson.JSON
import com.alibaba.fastjson.serializer.SerializerFeature
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.ArrayNode
import com.kuainiu.beidou.engine.component.domain.{LabelSchemaItem, LabelType, MapParams, SampleInfo, StoreEngine}
import com.kuainiu.beidou.engine.component.impl.DomainConverter
import com.kuainiu.beidou.engine.component.utils.JsonUtils
import org.apache.commons.collections.MapUtils

import scala.collection.JavaConverters._
import scala.collection.mutable
import scala.util.parsing.json.JSONObject

object As {
  def main(args: Array[String]): Unit = {

    val ss = """{"st":{"retainFeature":false,"typeProbe":true,"splitFeatures":["features"]},"in":[{"schema":{"ms":true,"items":[{"partition":false,"name":"id","tag":null,"msType":"continuous","type":"int","desc":null},{"partition":false,"name":"features","tag":null,"msType":"textValue","type":"map<string,string>","desc":null},{"partition":false,"name":"score","tag":null,"msType":"continuous","type":"int","desc":null},{"partition":false,"name":"qualified","tag":null,"msType":null,"type":"int","desc":null}]},"dType":null,"dsType":null,"comment":null,"label":{"partition":false,"functionSrc":null,"labelType":"label","name":"qualified","tag":null,"msType":null,"type":"int","desc":null},"storeInfo":{"storeGranularity":null,"storeEngine":"hdfs","db":null,"target":"/user/model/phecda/dataset/27930d1a-f998-481e-98af-7f64a0241da1"},"time":null,"fileType":"csv","pLabel":null}],"ctx":{"nodeSubType":"main","flowId":34,"nodeId":126,"uniqueId":"2536020000000000"},"out":[]}""".stripMargin

    val a = JsonUtils.toJavaEntity(ss, classOf[MapParams])
    val pol = DomainConverter.getDataProtocols(a)
    println(pol)

//    val axxx = new LabelSchemaItem(null, null, null)

//    println(a.getMapParams("st"))
//    val a1 = a.getJSONObject("st").getJSONArray("splitFeatures").toJavaList(classOf[String]).asScala
//    println(a1)
//
//    JSON.toJSONString(JsonUtils.toJsonObject(ss), SerializerFeature.DisableCircularReferenceDetect)
//
//
//    val a113 = StoreEngine.withName("hdfs")
//    println(a113 == StoreEngine.HDFS)
//    println(a113 == StoreEngine.HIVE)

//    val map = JsonUtils.toScalaMap[String, Object](ss).toMap
//    println(map)
//MapParams
//    val s1 = Map("xxx" -> Array("1", "2", "3"), "xx2" -> Array("xxxx", "ccccc"))
//    val ss1 = JsonUtils.scalaToString(s1)
//    println(ss1)
//
//    val aa1 = JsonUtils.toScalaMap(ss1)
//    println(aa1)
//
//
//    val uu1 = JsonUtils.scalaToString(Array("xxxx", "ccccc"))
//    val uu2 = JsonUtils.toJavaList[String](uu1).asScala




//    val mp = JsonUtils.toJsonNode(ss)
//    val aa = mp.get("st").get("splitFeatures").asInstanceOf[ArrayNode].elements()
//
//    var cc = Spliterators.spliteratorUnknownSize(aa, 0)
//    val ccv =                            StreamSupport.stream(cc, false).asScala
////    println(mp.get("st").get.asInstanceOf[mutable.HashMap[String, AnyRef]].get("typeProbe").get)
//
//                val yy = ccv.map(x => {""}).collect(Collectors.toList())
//
//
//    JSON
  }
}
