package com.kuainiu.beidou.engine.component.impl

import com.alibaba.fastjson.{JSONArray, JSONObject}
import com.kuainiu.beidou.engine.component.domain.StoreEngine.StoreEngine
import com.kuainiu.beidou.engine.component.domain.{DataProtocol, DataProtocols, DataSchema, DataSchemaItem, FileType, LabelSchemaItem, LabelType, MapParams, MsType, SampleInfo, StoreEngine, StoreInfo, TimeSchemaItem}

import scala.collection.JavaConverters._
import scala.collection.mutable.ArrayBuffer

object DomainConverter {

  /**
   * JSON对象转换
   * @param arrays  JSON数组
   * @return
   */
  def jsonArrayToScala(arrays: JSONArray): Array[JSONObject] = {
    if(arrays != null){
      arrays.asScala.map(item=> item.asInstanceOf[JSONObject]).filter(item => item != null && !item.isEmpty).toArray
    }
    else{
      Array()
    }
  }

  /**
   * 从协议中获取数据协议
   * @param params
   * @return
   */
  def getDataProtocols(params: MapParams): DataProtocols = {
    val dataProtocols = new DataProtocols()
    if(params != null && !params.isEmpty) {
      val ins: JSONArray = params.getJSONArray("in")
      if(ins != null && !ins.isEmpty){
        jsonArrayToScala(ins).foreach(item => {
          //获取存储信息
          var storeInfo: StoreInfo = null
          //获取文件类型
          val fileType = FileType.withName(item.getString("fileType"))
          //获取存储信息
          val storeInfoJson = item.getJSONObject("storeInfo")
          if(storeInfoJson != null){
            //解析存储信息
            val db = storeInfoJson.getString("db")
            val target = storeInfoJson.getString("target")
            val storeEngine: StoreEngine = StoreEngine.withName(storeInfoJson.getString("storeEngine"))
            //解析Schema信息
            var schema: DataSchema = null
            val schemaJson = item.getJSONObject("schema")
            if(schemaJson != null){
              //预处理
              val schemaItemsJson = schemaJson.getJSONArray("items")
              if(schemaItemsJson != null && !schemaItemsJson.isEmpty){
                val schemaItems: ArrayBuffer[DataSchemaItem] = ArrayBuffer[DataSchemaItem]()
                jsonArrayToScala(schemaItemsJson).foreach(item => {
                  //解析单个元数据信息
                  val name = item.getString("name")
                  val dataType = item.getString("type")
                  val desc = item.getString("desc")
                  val msTypeStr = item.getString("msType")
                  schemaItems += new DataSchemaItem(name, dataType, desc, MsType.fromName(msTypeStr))
                })
                schema = new DataSchema(schemaItems.toArray)
              }
            }
            //组织当前的存储信息
            storeInfo = new StoreInfo(storeEngine, fileType, db, target, schema)
          }
          //获取样本信息
          var sampleInfo: SampleInfo = null
          val labelJson = item.getJSONObject("label")
          val timeJson = item.getJSONObject("time")
          if((labelJson != null && !labelJson.isEmpty) || (timeJson != null && !timeJson.isEmpty)){
            var label: LabelSchemaItem = null
            if(labelJson != null && !labelJson.isEmpty){
              val dataType = labelJson.getString("type")
              val name = labelJson.getString("name")
              val desc = labelJson.getString("desc")
              val msTypeStr = labelJson.getString("msType")
              val labelType = labelJson.getString("labelType")
              val functionSrc = labelJson.getString("functionSrc")
              if(labelType == null || labelType.isEmpty){
                label = new LabelSchemaItem(name, dataType,desc, MsType.fromName(msTypeStr))
              }
              else{
                label = new LabelSchemaItem(name, dataType,desc, MsType.fromName(msTypeStr), LabelType.withName(labelType), functionSrc)
              }
            }
            var time: TimeSchemaItem = null
            if(timeJson != null && !timeJson.isEmpty){
              val name = timeJson.getString("name")
              val dataType = timeJson.getString("type")
              val desc = timeJson.getString("desc")
              time = new TimeSchemaItem(name, dataType, desc)
            }
            sampleInfo = new SampleInfo(label, time)
          }
          //添加数据
          dataProtocols.add(new DataProtocol(storeInfo, sampleInfo))
        })
      }
    }

    dataProtocols
  }

}
