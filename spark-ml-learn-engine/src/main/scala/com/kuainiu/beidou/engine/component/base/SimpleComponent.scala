package com.kuainiu.beidou.engine.component.base

import com.kuainiu.beidou.engine.component.core.DataLoad
import com.kuainiu.beidou.engine.component.domain.{DataInfos, MapParams}
import com.kuainiu.beidou.engine.component.impl.DomainConverter
import org.apache.spark.sql.{Dataset, SparkSession}

/**
 *简单封装,用于上层使用
 */
abstract class SimpleComponent extends ScalaComponent{

  /**
   * 训练流程
   */
  def train(spark: SparkSession, params: MapParams): String = {
    this.train(DataLoad.loadDataInfos(spark, DomainConverter.getDataProtocols(params)), params)
  }

  /**
   * 数据转换流程
   */
  def transform(spark: SparkSession, params: MapParams): String = {
    this.transform(DataLoad.loadDataInfos(spark, DomainConverter.getDataProtocols(params)), params)
  }

  /**
   * 训练流程
   */
  def train(dataInfos: DataInfos, params: MapParams): String

  /**
   * 数据转换流程
   */
  def transform(dataInfos: DataInfos, params: MapParams): String

}
