package com.kuainiu.beidou.engine.component.utils

import com.alibaba.fastjson.{JSON, JSONArray, JSONObject, TypeReference}
import com.alibaba.fastjson.serializer.SerializerFeature
import org.json4s.jackson.JsonMethods._
import org.json4s.jackson.Serialization
import org.json4s.{NoTypeHints, _}
import scala.collection.JavaConverters._

/**
 * <p>
 * 时间：2020-04-08 10:00
 * <p>
 * 描述：Scala中的JSON处理工具
 * <p>
 * 作者： 吴德文
 * <p>
 * 版权： Copyright © 2019 快牛金科版权所有
 **/
object JsonUtils {

  private def isEmpty(str :String): Boolean ={
    str == null || str.isEmpty
  }

  /**
   * 将Scala对象转化为字符串
   * @param bean  对象
   * @return  json字符串
   */
  def scalaToString[A <: AnyRef](bean: AnyRef): String = {
    Serialization.write(bean)(Serialization.formats(NoTypeHints))
  }

  /**
   * 将字符串转换为ScalaEntity
   * @param json  数据字符串
   * @param clazz Entity class
   * @return  Entity
   */
  def toScalaEntity[T](json: String, clazz: Class[T])(implicit m: Manifest[T]): T = {
    implicit val formats: DefaultFormats.type = DefaultFormats
    parse(json).extract[T]
  }

  /**
   * 将字符串转换为Map
   * @param json  数据字符串
   * @return
   */
  def toScalaMap[keyClass, valueClass](json: String): Map[keyClass, valueClass] = {
    this.toJavaMap[keyClass, valueClass](json).asScala.toMap
  }

  /**
   * 将字符串转换为List
   * @param json  数据字符串
   * @return
   */
  def toScalaList[valueClass](json: String): List[ valueClass] = {
    this.toJavaList[valueClass](json).asScala.toList
  }

  /**
   * 将Bean转化为json字符串[不格式化]
   * @param bean bean对象
   * @return json
   */
  def javaToString(bean: Any): String = {
    javaToString(bean, pretty = false)
  }

  /**
   * 将Bean转化为json字符串
   * @param bean    bean对象
   * @param pretty  是否格式化
   * @return json
   */
  def javaToString(bean: Any, pretty: Boolean): String = {
    if (bean == null) {
      null.asInstanceOf[String]
    }
    else bean match {
      case str: String =>
        str
      case _ =>
        if (!pretty) {
          JSON.toJSONString(bean, SerializerFeature.DisableCircularReferenceDetect)
        }
        else {
          JSON.toJSONString(bean, SerializerFeature.DisableCircularReferenceDetect, SerializerFeature.PrettyFormat)
        }
    }
  }

  /**
   * 将字符串转换为Entity
   * @param json  数据字符串
   * @param clazz Entity class
   * @return
   */
  def toJavaEntity[T](json: String, clazz: Class[T]): T = {
    if (isEmpty(json)) {
      null.asInstanceOf[T]
    }
    else {
      JSON.parseObject(json, clazz)
    }
  }

  /**
   * 将字符串转换为Map
   * @param json  数据字符串
   * @return
   */
  def toJavaMap[keyClass, valueClass](json: String): java.util.Map[keyClass, valueClass] = {
    if (isEmpty(json)) {
      null.asInstanceOf[java.util.Map[keyClass, valueClass]]
    }
    else {
      JSON.parseObject(json, new TypeReference[java.util.Map[keyClass, valueClass]](){})
    }
  }

  /**
   * 将字符串转换为List
   * @param json  数据字符串
   * @return
   */
  def toJavaList[valueClass](json: String): java.util.List[valueClass] = {
    if (isEmpty(json)) {
      null.asInstanceOf[java.util.List[valueClass]]
    }
    else {
      JSON.parseObject(json, new TypeReference[java.util.List[valueClass]](){})
    }
  }

  /**
   * 将字符串转换为Entity
   * @param json  数据字符串
   * @return
   */
  def toJsonObject(json: String): JSONObject = {
    if (isEmpty(json)) {
      null.asInstanceOf[JSONObject]
    }
    else {
      JSON.parseObject(json)
    }
  }

  /**
   * 将字符串转换为Entity
   * @param json  数据字符串
   * @return
   */
  def toJsonArray(json: String): JSONArray = {
    if (isEmpty(json)) {
      null.asInstanceOf[JSONArray]
    }
    else {
      JSON.parseArray(json)
    }
  }

}
