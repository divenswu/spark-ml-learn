package com.kuainiu.beidou.engine.component.domain

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.types.StructType

class DataInfo(val dataFrame: DataFrame, val schema: StructType) {

}
