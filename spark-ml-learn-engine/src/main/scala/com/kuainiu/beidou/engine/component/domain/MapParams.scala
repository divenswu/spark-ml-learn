package com.kuainiu.beidou.engine.component.domain

import com.alibaba.fastjson.serializer.SerializerFeature
import com.alibaba.fastjson.{JSON, JSONObject}

/**
 * 参数信息对象
 */
class MapParams extends JSONObject {

  def getMapParams(key:String): MapParams = {
    val value = this.getJSONObject(key)
    if(value != null){
      JSON.parseObject(JSON.toJSONString(value, SerializerFeature.PrettyFormat), classOf[MapParams])
    }
    else{
      null.asInstanceOf[MapParams]
    }
  }

}
