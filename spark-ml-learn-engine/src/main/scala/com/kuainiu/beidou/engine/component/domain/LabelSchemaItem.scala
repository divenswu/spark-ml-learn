package com.kuainiu.beidou.engine.component.domain

import com.kuainiu.beidou.engine.component.domain.LabelType.LabelType

class LabelSchemaItem(name:String, dataType: String, desc: String, msType: MsType, val labelType: LabelType, val functionSrc: String) extends DataSchemaItem(name, dataType, desc, msType) {

  def this(name:String, dataType: String, desc: String) {
    this(name, dataType, desc, null, LabelType.LABEL, null)
  }

  def this(name:String, dataType: String, desc: String, msType: MsType) {
    this(name, dataType, desc, msType, LabelType.LABEL, null)
  }

  def this(name:String, dataType: String, desc: String, msType: MsType, labelType: LabelType) {
    this(name, dataType, desc, msType, labelType, null)
  }

}
