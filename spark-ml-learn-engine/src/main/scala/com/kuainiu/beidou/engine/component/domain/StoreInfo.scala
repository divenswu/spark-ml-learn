package com.kuainiu.beidou.engine.component.domain

import com.kuainiu.beidou.engine.component.domain.FileType.FileType
import com.kuainiu.beidou.engine.component.domain.StoreEngine.StoreEngine

/**
 * 数据存储模型
 * @param storeEngine
 * @param fileType
 * @param db
 * @param target
 * @param schema
 */
class StoreInfo(val storeEngine: StoreEngine, val fileType: FileType, val db: String, val target: String, val schema: DataSchema){

}
