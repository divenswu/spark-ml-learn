package com.kuainiu.beidou.engine.component.impl

import com.kuainiu.beidou.engine.component.base.{SimpleComponent, SingleComponent}
import com.kuainiu.beidou.engine.component.domain.{DataInfo, DataInfos, MapParams}
import com.kuainiu.beidou.engine.component.utils.JsonUtils
import org.apache.spark.sql.SparkSession

object BaseEncoderComponent {
  def main(args: Array[String]): Unit = {
    val strPol = """{"st":{"retainFeature":false,"typeProbe":true,"splitFeatures":["features"]},"in":[{"schema":{"ms":true,"items":[{"partition":false,"name":"id","tag":null,"msType":"continuous","type":"int","desc":null},{"partition":false,"name":"features","tag":null,"msType":"textValue","type":"map<string,string>","desc":null},{"partition":false,"name":"score","tag":null,"msType":"continuous","type":"int","desc":null},{"partition":false,"name":"qualified","tag":null,"msType":null,"type":"int","desc":null}]},"dType":null,"dsType":null,"comment":null,"label":{"partition":false,"functionSrc":null,"labelType":"label","name":"qualified","tag":null,"msType":null,"type":"int","desc":null},"storeInfo":{"storeGranularity":null,"storeEngine":"hdfs","db":null,"target":"/user/model/phecda/dataset/27930d1a-f998-481e-98af-7f64a0241da1"},"time":null,"fileType":"csv","pLabel":null}],"ctx":{"nodeSubType":"main","flowId":34,"nodeId":126,"uniqueId":"2536020000000000"},"out":[]}""".stripMargin
    val spark = SparkSession.builder().appName(this.getClass.getSimpleName).master("local[3]").getOrCreate()
    val params = JsonUtils.toJavaEntity(strPol, classOf[MapParams])

    new BaseEncoderComponent().train(spark, params)
  }
}

class BaseEncoderComponent extends SingleComponent{

  /**
   * 训练流程
   */
  override def train(dataInfo: DataInfo, params: MapParams): String = {
    dataInfo.dataFrame.show()
    "success"
  }

  /**
   * 数据转换流程
   */
  override def transform(dataInfo: DataInfo, params: MapParams): String = {

    "error"
  }

}


