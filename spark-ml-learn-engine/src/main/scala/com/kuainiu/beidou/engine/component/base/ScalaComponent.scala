package com.kuainiu.beidou.engine.component.base

import com.kuainiu.beidou.engine.component.domain.MapParams
import org.apache.spark.sql.{Dataset, SparkSession}

/**
 * Spark底层接口封装
 */
trait ScalaComponent extends Serializable {

  /**
   * 训练流程
   */
  def train(spark: SparkSession, params: MapParams): String

  /**
   * 数据转换流程
   */
  def transform(spark: SparkSession, params: MapParams): String

}
