package com.kuainiu.beidou.engine.component.domain

/**
 * 元数据信息的基本单元
 * @param name
 * @param dataType
 * @param desc
 * @param msType
 */
class DataSchemaItem(val name:String, val dataType: String, val desc: String, val msType: MsType){

}
