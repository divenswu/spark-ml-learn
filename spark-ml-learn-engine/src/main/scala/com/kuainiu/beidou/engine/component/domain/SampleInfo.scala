package com.kuainiu.beidou.engine.component.domain

/**
 * 样本信息
 */
class SampleInfo(val label: LabelSchemaItem, val time: TimeSchemaItem = null) {

}
