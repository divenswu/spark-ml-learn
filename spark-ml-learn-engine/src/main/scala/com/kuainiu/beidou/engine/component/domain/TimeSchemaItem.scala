package com.kuainiu.beidou.engine.component.domain

class TimeSchemaItem (name:String, dataType: String, desc: String, msType: MsType) extends DataSchemaItem(name, dataType, desc, msType){

  def this(name:String, dataType: String) {
    this(name, dataType, null, null)
  }

  def this(name:String, dataType: String, desc: String) {
    this(name, dataType, desc, null)
  }

}
