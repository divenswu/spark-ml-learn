package com.kuainiu.beidou.engine.component.domain

/**
 * 存储引擎枚举
 */
object StoreEngine extends Enumeration{

  type StoreEngine = Value

  val HIVE = Value("hive")

  val HDFS = Value("hdfs")

  val LOCAL= Value("local")

  val UNKNOWN= Value("unknown")

}
