package com.diven.spark.ml.learn.feature

import org.apache.spark.sql.types.{DataType, DataTypes}

import scala.collection.mutable.ArrayBuffer

object Test {
  def main(args: Array[String]): Unit = {
    println(DataTypes.StringType.json)
    println(DataTypes.createMapType(DataTypes.StringType, DataTypes.StringType).json)
    val names = ArrayBuffer[String]()
    names += "xxxx"
    names += "xxxxs"

    println(names.mkString(","))

    var value = Map[String, DataType]()
    value += ("xxx"-> DataTypes.StringType)
    println(value)
  }
}
